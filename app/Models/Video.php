<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $cover_url
 * @property string $video_url
 * @property string $info
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Video extends Model
{
    use HasFactory;

    protected $table = 'videos';

    protected $fillable = [
        'cover_url',
        'video_url',
        'info',
    ];
}
