<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function successData($data, $message = '', $code = 200)
    {
        return response()->json([
            'status'    => true,
            'message'   => $message,
            'data'      => $data
        ], $code);
    }

    protected function successMessage($message)
    {
        return response()->json([
            'status'    => true,
            'message'   => $message,
            'data'      => []
        ]);
    }

    protected function fail($message, $code = 200)
    {
        return response()->json([
            'status'    => false,
            'message'   => $message,
            'data'      => []
        ], $code);
    }
}
