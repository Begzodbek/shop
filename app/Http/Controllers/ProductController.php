<?php

namespace App\Http\Controllers;

use App\Http\Requests\PerPageRequest;
use App\Models\Product;
use App\Models\User;

class ProductController extends Controller
{
    public function all()
    {
        return $this->successData(Product::query()->get());
    }

    public function index(PerPageRequest $request)
    {
        return $this->successData(Product::query()->paginate($request->per_page));
    }

    public function view(User $user)
    {
        return $this->successData($user->products);
    }
}
