<?php

namespace App\Http\Controllers;

use App\Http\Requests\PerPageRequest;
use App\Models\User;

class UserController extends Controller
{
    public function all()
    {
        return $this->successData(User::query()->get());
    }

    public function index(PerPageRequest $request)
    {
        return $this->successData(User::query()->paginate($request->per_page));
    }

    public function view(User $user)
    {
        return $this->successData($user);
    }

}
