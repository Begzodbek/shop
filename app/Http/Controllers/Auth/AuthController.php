<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    public function login(LoginRequest $request)
    {
        if (!$token = auth()->attempt([
            'email' => $request->username,
            'password' => $request->password,
        ])) {
            return $this->fail('Unauthorized', 401);
        }

        return $this->createNewToken($token);
    }

    public function register(LoginRequest $request)
    {
        $user = User::query()->create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        return $this->successData(['user' => $user], 'User successfully registered', 201);
    }

    public function logout()
    {
        auth()->logout();
        return $this->successMessage('User successfully signed out');
    }

    public function refresh()
    {
        return $this->createNewToken(auth()->refresh());
    }

    public function userProfile()
    {
        return $this->successData(auth()->user());
    }

    protected function createNewToken($token)
    {
        return $this->successData([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()
        ]);
    }
}
