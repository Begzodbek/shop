docker-up:
	docker-compose up -d

docker-down:
	docker-compose down

docker-build:
	docker-compose up --build -d

test:
	docker-compose exec php-cli vendor/bin/phpunit

composer-install:
	docker-compose exec php-cli composer install

migrate:
	docker-compose exec php-cli php artisan migrate

migrate-down:
	docker-compose exec php-cli php artisan migrate:rollback

seed:
	docker-compose exec php-cli php artisan db:seed

perm:
	sudo chmod -R 777 bootstrap/cache storage vendor
