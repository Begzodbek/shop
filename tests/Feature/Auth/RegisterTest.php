<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    public function testForm(): void
    {
        $response = $this->get('/api/register');

        $response->assertStatus(405)
            ->assertSee('Register');
    }

    public function testErrors(): void
    {
        $response = $this->post('/api/register', [
            'name' => '',
            'email' => '',
            'password' => '',
            'password_confirmation' => '',
        ]);

        $response->assertStatus(400);
    }

    public function testSuccess(): void
    {
        $user = User::factory()->make();

        $response = $this->post('/api/register', [
            'name' => $user->name,
            'email' => $user->email,
            'password' => 'password',
            'password_confirmation' => 'password',
        ]);

        $response->assertStatus(201);
    }
}
