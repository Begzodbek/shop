<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Tests\TestCase;

class LoginTest extends TestCase
{
    public function testForm(): void
    {
        $response = $this->get('/api/login');

        $response
            ->assertStatus(405)
            ->assertSee('Login');
    }

    public function testErrors(): void
    {
        $response = $this->post('/api/login', [
            'email' => '',
            'password' => '',
        ]);

        $response->assertStatus(422);
    }

    public function testSuccess(): void
    {
        $user = User::factory()->create();

        $response = $this->post('/api/login', [
            'username' => $user->email,
            'password' => 'password',
        ]);

        $response->assertStatus(200);
    }
}
