# HKeeper

## Run this command to use project

- ```sudo make docker-build```
- ```sudo make composer-install```
- ```sudo make perm``` to give the permission some files for use it
- You need to create ```.env``` file using ```.env.example```
- ```sudo make migrate``` for create tables in database
- ```sudo make test``` if you want to test the project
- ```sudo make seed``` to create some test data in database

#### After doing this commands project will be run in ```http://127.0.0.1:8080/```, for test the API you can just open the ```http://127.0.0.1:8080/docs/index.html```

#### Test admin user:
- ```Username: admin@gmail.com```
- ```Password: password```
